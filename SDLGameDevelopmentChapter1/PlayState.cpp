//
//  PlayState.cpp
//  SDLGameDevelopmentChapter1
//
//  Created by Evan Chapman on 5/11/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "PlayState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "Game.h"
#include "TextureManager.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "LevelParser.h"

const string PlayState::s_playID = "PLAY";

PlayState::~PlayState(){
    cout << "Deleted play state\n";
}

void PlayState::update(){
    
    if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE)){
        Game::Instance()->getStateMachine()->pushState(new PauseState());
    }

    //Check collisions
    if ( !m_gameObjects.empty()){
        for (int i = 0 ; i < m_gameObjects.size(); i++){
            m_gameObjects[i]->update();
            if (checkCollision(m_gameObjects[0], m_gameObjects[1])){
                Game::Instance()->getStateMachine()->changeState(new GameOverState());
            }
        }
    }
  
    //If enter is pressed enter editmode
    if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_RETURN)){
        Game::Instance()->setEditMode(true);
    }
  
    //reload level in edit mode
    if (Game::Instance()->editModeEnabled()){
        onExit();
        onEnter();
        SDL_Delay(100);
    }
    
    pLevel->update();
    
}

void PlayState::render(){
    
    //Render Tiles
    pLevel->render();
    
//    //Render Sprites
//    for (int i = 0 ; i < m_gameObjects.size(); i++){
//        
//        if (m_gameObjects[i]->GetParams().getType() != string("Light")){
//            m_gameObjects[i]->draw();
//            cout << "Object render\n";
//        }
//    }
//    

}

bool PlayState::onEnter(){
    cout << "Entering play state\n";
    
    StateParser parser;
    parser.parseState("scripts/xm1.xml", s_playID,
                      &m_gameObjects, &m_textureIDList,&pLevelFile);
    
    LevelParser levelParser;
    
    pLevel = levelParser.parseLevel(pLevelFile.c_str());
    
    cout << "Entered play state\n";
    
    return true;
}

bool PlayState::onExit(){
    cout << "Exited play state\n";
    
    while (!m_gameObjects.empty()){
        cout << "Game Objects size: " << m_gameObjects.size() << endl;
        m_gameObjects.back()->clean();
        m_gameObjects.pop_back();
    }
    
    for (int i = 0 ; i < m_textureIDList.size() ; i++){
        TextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
    }
    
    cout << "Game Objects size: " << m_gameObjects.size() << endl;
    
    m_gameObjects.clear();
    
    TextureManager::Instance()->clearFromTextureMap("tiger_image");
    
    InputHandler::Instance()->reset();
    return true;
}

bool PlayState::checkCollision(GameObject *p1, GameObject *p2){
    int leftA,leftB;
    int rightA,rightB;
    int topA, topB;
    int bottomA,bottomB;
    
    leftA = p1->GetParams().getPosition().getX();
    leftB = p2->GetParams().getPosition().getX();
    
    rightA = p1->GetParams().getPosition().getX() + p1->GetParams().getWidth();
    rightB = p2->GetParams().getPosition().getX() + p2->GetParams().getWidth();
    
    topA = p1->GetParams().getPosition().getY();
    topB = p2->GetParams().getPosition().getY();
    
    bottomA = p1->GetParams().getPosition().getY() + p1->GetParams().getHeight();
    bottomB = p2->GetParams().getPosition().getY() + p2->GetParams().getHeight();
    
    //If any sides outside, no collision occured
    if (leftA >= rightB) { return false;}
    if (rightA <= leftB) { return false;}
    if (topA > bottomB) { return false;}
    if (bottomA  <= topB) { return false;}
    
    
    return true;
}









































































//END