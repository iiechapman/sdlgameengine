//
//  Level.cpp
//  SDLGameDevelopmentChapter1
//
//  Created by Evan Chapman on 6/23/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "Level.h"

vector<Tileset>* Level::getTilesets(){
    return &m_tilesets;
}
vector<Layer*>* Level::getLayers(){
    return &m_layers;
}

Level::Level(){
    
}

void Level::render(){
    for (int i = 0 ; i < m_layers.size() ; i++){
        m_layers[i]->render();
    }
}

void Level::update(){
    for (int i = 0 ; i <m_layers.size() ;i++){
        m_layers[i]->update();
    }
}