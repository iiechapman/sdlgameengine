//
//  Light.cpp
//  SDLGameDevelopmentChapter1
//
//  Created by Evan Chapman on 4/23/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "Light.h"
#include "InputHandler.h"

Light::Light(){
    cout << "Created new Light\n";
    m_params.setType("Light");
}

Light::Light(GameObjectParams params , int numberFrames)
:SDLGameObject(params){
    GetParams().setVelocity({1,0});
    GetParams().setMaxFrames(numberFrames);
}

void Light::load(GameObjectParams params){
    SDLGameObject::load(params);
}

void Light::draw(){
    SDLGameObject::draw();
}

void Light::update(){
    
    if (GetParams().getX() < 100){
        GetParams().setVelocity({GetParams().getVelocity().getX() * -1,0});
    }
    
    if (GetParams().getX() > 500){
        GetParams().setVelocity({GetParams().getVelocity().getX() * -1,0});
    }
    
    
    if (GetParams().getVelocity().getX() != 0){
        GetParams().setFrame(int((SDL_GetTicks()/100) % GetParams().getTotalFrames()));
    } else {
        GetParams().setFrame(0);
    }
    
    SDLGameObject::update();
}

void Light::clean(){
    SDLGameObject::clean();
    cout << "Cleaning Light\n";
}