//
//  Player.h
//  SDLGameDevelopmentChapter1
//
//  Created by Evan Chapman on 4/23/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __SDLGameDevelopmentChapter1__Player__
#define __SDLGameDevelopmentChapter1__Player__

#include <iostream>
#include "SDLGameObject.h"
#include "GameObjectFactory.h"

//inherit from sdlgameobject
class Player : public SDLGameObject {
public:
    Player();
    Player(GameObjectParams params, int numberFrames);
    void draw();
    void update();
    void clean();
    void load(GameObjectParams params);
    
private:
    Vector2D m_acceleration{0,0};
};



class PlayerCreator : public BaseCreator{
    GameObject* createGameObject() const{
        return new Player();
    }
    
    
};



#endif /* defined(__SDLGameDevelopmentChapter1__Player__) */
