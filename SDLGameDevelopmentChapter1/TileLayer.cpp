//
//  TileLayer.cpp
//  SDLGameDevelopmentChapter1
//
//  Created by Evan Chapman on 6/23/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "TileLayer.h"
#include "Game.h"

void TileLayer::render(){
    int x,y,x2,y2 = 0;
    
    x = m_position.getX() / m_tileSize;
    y = m_position.getY() / m_tileSize;
    
    x2 = int(m_position.getX()) % m_tileSize;
    y2 = int(m_position.getY()) % m_tileSize;
    
    for (int i = 0 ; i < m_numRows; i++){
        for (int j = 0 ; j < m_numColumns; j++){
            
            int id = m_tileIDs[i][j+x];
            
            if (id == 0){
                continue;
            }
            
            Tileset tileset = getTilesetByID(id);
            
            id--;
            
            //Calculated ratio zoom
//            Game::Instance()->setZoom
//            ((float)(Game::Instance()->getGameWidth() /
//                     ((float)m_numColumns *  (float)m_tileSize)));
            
            //Manual Zoom
            //Game::Instance()->setZoom(4);
            
        
            TextureManager::Instance()->drawTile
            (tileset.name, tileset.margin, tileset.spacing,
             (((j * m_tileSize) - x2)),(((i * m_tileSize) - y2)),
             (m_tileSize), (m_tileSize),
             (id - (tileset.firstGridID - 1)) / tileset.numColumns,
             (id - (tileset.firstGridID - 1)) % tileset.numColumns,
             Game::Instance()->getZoom(),Game::Instance()->getRenderer());
        }
    }
}

void TileLayer::update(){
    m_position += m_velocity;
    m_velocity.setX(1);
}

TileLayer::TileLayer(int tileSize, const vector<Tileset> &tilesets)
:m_tileSize(tileSize) , m_tilesets(tilesets),m_position(0,0), m_velocity(0,0)
{ 
}


Tileset TileLayer::getTilesetByID(int tileID){
    for (int i = 0; i < m_tilesets.size() ; i++){
        if (i + 1 <=m_tilesets.size() - 1){
            if (tileID >= m_tilesets[i].firstGridID &&
                tileID < m_tilesets[i+1].firstGridID){
                return m_tilesets[i];
            }
        } else {
            return m_tilesets[i];
        }
    }
    
    cout << "Did not find tileset, returning empty tileset\n";
    Tileset t;
    return t;
}





































































//END