//
//  Player.cpp
//  SDLGameDevelopmentChapter1
//
//  Created by Evan Chapman on 4/23/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#include "Player.h"
#include "InputHandler.h"

Player::Player(){
    cout << "Created player object\n";
    m_params.setType("Player");
}

Player::Player(GameObjectParams params , int numberFrames)
:SDLGameObject(params){
    GetParams().setMaxFrames(numberFrames);
}

void Player::load(GameObjectParams params){
    SDLGameObject::load(params);
}

void Player::draw(){
    SDLGameObject::draw();
}

void Player::update(){
    
    //Update animation
    if (GetParams().getVelocity().getX() != 0){
        GetParams().setFrame
        (int((SDL_GetTicks()/ (1000 / GetParams().getAnimSpeed())) % GetParams().getTotalFrames()));

    } else {
        GetParams().setFrame(0);
    }
    
    //Mouse control
    if (InputHandler::Instance()->getMouseButtonState(RIGHT)){
        Vector2D* vec = InputHandler::Instance()->getMousePosition();
        
        GetParams().setVelocity((*vec - GetParams().getPosition()) / 50 );
        
        GetParams().getAcceleration().setX(.1);
        
        //Keyboard control
    } else if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT)){
        GetParams().getAcceleration().setX(.1);
    } else if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT)){
        GetParams().getAcceleration().setX(-.1);
    } else if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP)){
        GetParams().getAcceleration().setY(-.1);
    } else if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN)){
        GetParams().getAcceleration().setY(.1);
    } else {
        //stop all movement
        GetParams().getAcceleration().setX(0);
        GetParams().getAcceleration().setY(0);
        GetParams().getVelocity().setX(0);
        GetParams().getVelocity().setY(0);
    }
    
    
    SDLGameObject::update();
}

void Player::clean(){
    SDLGameObject::clean();
    cout << "Cleaning player\n";
}








