//
//  Level.h
//  SDLGameDevelopmentChapter1
//
//  Created by Evan Chapman on 6/23/14.
//  Copyright (c) 2014 Evan Chapman. All rights reserved.
//

#ifndef __SDLGameDevelopmentChapter1__Level__
#define __SDLGameDevelopmentChapter1__Level__

#include <iostream>
#include <string>
#include <vector>
#include "Layer.h"

using namespace std;

struct Tileset{
    int firstGridID;
    int tileWidth;
    int tileHeight;
    int spacing;
    int margin;
    int width;
    int height;
    int numColumns;
    string name;
};


class Level{
public:
    ~Level();
    
    void update();
    void render();
    
    vector<Layer*>* getLayers();
    vector<Tileset>* getTilesets();
    
private:
    Level();
    
    vector<Tileset> m_tilesets;
    vector<Layer*> m_layers;
    friend class LevelParser;
};


#endif /* defined(__SDLGameDevelopmentChapter1__Level__) */
